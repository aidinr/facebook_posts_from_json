var url = 'https://graph.facebook.com/MetLeesburg/posts?access_token=981005461951182|kIrcH7lT6qe0sKR26RnhB2Vozas';
$.ajax({
    type: 'GET',
    url: url,
    async: false,
    contentType: 'application/json',
    dataType: 'jsonp',
    success: function(result) {
        getPosts(result);
        links();
        $('.single-item').slick();
    },
    error: function(e) {
        console.log('error');
    }
});

function getPosts(obj) {
    $.each(obj.data, function () {
        var postDate = new Date(this.created_time).toLocaleString();
        var article = this.message;
        if(article === undefined) {
            article = this.story;
        }
        $('#fb-posts').append('<div>' + article + '<span>' + ' +---+ ' + postDate + '</span></div>');
    });
}

function links() {
    $('#fb-posts div').each(function(){
        var words = $(this).text().split(' ');
        for(i in words) {
            if(words[i].indexOf('http://') > -1) {
                words[i] = '<a href="' + words[i] + '">' + words[i] + '</a>';
            } else if(words[i].indexOf('+---+') > -1) {
                words[i] = '<span>' + words[i] + '</span>';
            } 
        }
        $(this).html(words.join(' '));
    });
    $('#fb-posts div').find(":contains('+---+')").each(function(){
        $(this).text($('span').html('<br>'));
        $(this).find(":contains('</span>')").after().wrap('<p></p>');
    });
}
